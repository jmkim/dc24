"""
Use this script to wipe all schedule data, e.g. when testing changes to
schedule-grid.yml
"""
from wafer.schedule.models import *


def run():
    for model in [ScheduleItem, Slot, ScheduleBlock, Venue]:
        model.objects.all().delete()
