from django.urls import include, re_path, path
from dc24.views import now_or_next, invitation_request_form, prometheus_metrics


urlpatterns = [
    re_path(r'^now_or_next/(?P<venue_id>\d+)/$', now_or_next, name="now_or_next"),
    path('about/visas/request/', invitation_request_form, name="invitation_request_form"),
    path('prometheus-metrics/', prometheus_metrics, name="prometheus_metrics"),
]
