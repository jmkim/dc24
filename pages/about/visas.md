---
name: Visas
---

# Korean Visas and K-ETA

South Korea offers Visa waiver program to many number of countries. If you're visiting South Korea for short-term (up to 90 days) and eligible for visa waiver program, Normally you'll be required to apply for K-ETA. In case you need Visa to visit South Korea, You'll most likely need [C-3-1(Short-Term General) Visa](https://www.visa.go.kr/openPage.do?MENU_ID=1010201) which can be used to participate meetings and conferences.

To check if you need Visa to visit South Korea, Visit [Visa Navigator on visa.go.kr](https://www.visa.go.kr/openPage.do?MENU_ID=10101).

<div class="alert alert-dark border border-warning" role="alert">
  <i class="fa fa-warning" aria-hidden="true"></i>
  <span class="bold"> WARNING: </span>
  Beware of unauthentic/fraudulent websites. Make sure you follow the official
  government website
  <a href="https://www.visa.go.kr/">WWW.VISA.GO.KR</a> for Visa application and
  <a href="https://www.k-eta.go.kr/">WWW.K-ETA.GO.KR</a> for K-ETA application.
</div>

## K-ETA

Travellers who can visit South Korea without Visa are required to have a K-ETA (electronic travel authorization) issued in advance. Visitors in the following categories are not required to apply for K-ETA or are exempted from K-ETA:

- People holding a passport from the following countries who plan to visit South Korea by the end of 2024 - [see notice on k-eta.go.kr](https://www.k-eta.go.kr/portal/board/viewboarddetail.do?bbsSn=149899)
    - Australia, Austria, Belgium, Canada, Denmark, Finland, France, Germany, Hong Kong, Italy, Japan, Macao, Netherlands, New Zealand, Norway, Poland, Singapore, Spain, Sweden, Taiwan, UK, US (including Guam)
    - The official announcement only list the above 22 countries. But actually it is known that people from following countries are also temporarily exempted from K-ETA: Andorra, Bahrain, Brunei, Bulgaria, Chile, Colombia, Croatia, Cyprus, Czech Republic, Estonia, Federated States of Micronesia, Fiji, Greece, Hungary, Iceland, Ireland, Israel, Kiribati, Kuwait, Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Marshall Islands, Mexico, Monaco, Nauru, Oman, Palau, Portugal, Qatar, Romania, Samoa, San Marino, Saudi Arabia, Slovakia, Slovenia, Solomon Islands, South Africa, Switzerland, Tonga, Tuvalu, United Arab Emirates, Vatican. However, they are not in the announcement, so it's best to apply yourself to make sure. If you qualify for an exemption, you won't pay a fee.
- Travellers aged 17 years old or younger, or 65 years old or older at the time of arrival - [See FAQ on k-eta.go.kr](https://www.k-eta.go.kr/portal/board/viewboardlist.do?tmpltNm=faq)
- Holders of a valid Diplomatic passport, official passport, APEC Business Travel Card or UN Passport
- Flight attendants and Seafarers
- Active service members of the United States Armed Forces according to the US-ROK Status of Forces Agreement (SOFA)
    - Note: Dependents of USFK members and civilian component, and contractors are also K-ETA exempted but need to obtain approval for K-ETA exemption in advance.

If you are required to apply for a K-ETA, [go to application form webpage](https://www.k-eta.go.kr/portal/apply/viewstep1.do) and start preparing your application. You may check your application result [here.](https://www.k-eta.go.kr/portal/apply/viewapplysearch.do)

- Your K-ETA must be issued before taking a flight or ferry to South Korea. K-ETA approval normally only takes a few hours, but we **strongly** recommended applying at least 72 hours in advance in case of delays.
- Be sure to double-check all information before submitting your form, since your personal (or identity) information such as names, passport numbers cannot be edited after submission. Even with incorrect personal information, your application might be approved but your K-ETA remains invalid. If that happens, you'll need to apply again with another fee payment. 
- Application guide can be found [here](https://www.k-eta.go.kr/portal/guide/viewetaapplication.do).
- Application fee is 10,000 KRW (approx. 8 USD). Fee is not refundable. More information on fee and payment can be found [here.](https://www.k-eta.go.kr/portal/guide/viewetafeeinformation.do)

## Preparing and Applying for C-3-1 Visa

To visit South Korea for DebConf24 with a visa, a C-3 visa (C-3-1 Short-Term General or C-3-9 Ordinary Tourist) should be sufficient. Among C-3 series of visa, we recommend applying for C-3-1 visa to make your purpose of visit clearer. 

If you need a C-3-1 visa and therefore an invitation from the conference sponsors, we recommend you register to attend and then let us know as early as possible. This is because visa applications usually take 2 weeks or more, and sometimes we need to send you document printouts physically via the postal service.

### What you (the attendee) will need to prepare

To obtain this kind of visa, you will need to prepare the following documents. The required documents vary by residency and passport authority, so these are general guidelines. Check the visa application documentation carefully for your particular circumstances.

- Your passport with more than 6 month of validity (Copy, original or both depending on diplomatic mission)
- Recently taken passport photo (1 or more depending on diplomatic mission)
- [Visa application form](https://www.visa.go.kr/downfile/VisaapplicationForm_EN.pdf) (Depending on diplomatic mission: printed then handwritten, or information filled in then printed, or [prepared with e-Form](https://www.visa.go.kr/openPage.do?MENU_ID=10204))
- Documents that shows your financial ability to travel South Korea.
    - If you own a business: Business profile, Business registration or both
    - If you're an employee: Certificate of employment and Salary slip
    - If you're a student: Certificate of enrollment
    - Recent bank statement and income tax return, personal (for employee) or for your business (for business owners)
        - Could be required or not and required period range vary depending on the diplomatic mission.
- Documents that show your purpose of visiting South Korea (may be required or not depending on the diplomatic mission)
    - Cover letter that includes your plan in South Korea, travel funding details and more.
    - Booking confirmation of accommodation and round trip flight or ferry ticket.

### What we (organizers) will provide to attendee for visa application and how to request

You will also need to receive following documents below either via email or via postal service from us:

- Invitation letter
- Letter of Guarantee (if the diplomatic mission requires this)
- Business registration
- Event brochure

We can provide the documents above once your registration is confirmed. To request, please complete registration first, then please fill out the visa request form. After completing the form, Click "Send Email" button. This will open email client or email app on your desktop or mobile with content already filled in. Attach copy of your passport and other documents we requested then complete the request by sending the mail.

[**Click here to submit visa invitation request with form**](/about/visas/request)

Note: In case you are under the age of 18 (not an adult yet) and will be accompanied by a parent, Visa team will also need the same information for the accompanying person.

As we are requesting you to send personally identifying (and potentially sensitive) information, you can use the `36CC 0658 7B29 9651 A63A  F2FF C97D 198E B3C2 BC0E` OpenPGP key to communicate with the visa team. We inline here the public key for your convenience:

	-----BEGIN PGP PUBLIC KEY BLOCK-----
	
	mDMEZiqBTBYJKwYBBAHaRw8BAQdAbKZT/6V7SvXBGVPuritd3s7DpwaJYiA1crJ/
	JNLjKJOI0QQfFgoAgwWCZiqBTAWJAUiytAMLCQcJEMl9GY6zwrwORxQAAAAAAB4A
	IHNhbHRAbm90YXRpb25zLnNlcXVvaWEtcGdwLm9yZ8Q3kgmkWayCGTP5AXrTjVVB
	k8B+VLsZu79bFu+pd0T/AxUKCAKbAQIeARYhBDbMBlh7KZZRpjry/8l9GY6zwrwO
	AABMKQEA6D0OGsf2svYiqQqV/hpLsDxig8StTXj4kEyQU1PxkowA/37cAv4rPFcw
	VTw7sx+2nGgJgne18NdpNQo1R0psgCsAtCZEZWJDb25mMjQgVmlzYSB0ZWFtIDx2
	aXNhQGRlYmNvbmYub3JnPojUBBMWCgCGBYJmKoFMBYkBSLK0AwsJBwkQyX0ZjrPC
	vA5HFAAAAAAAHgAgc2FsdEBub3RhdGlvbnMuc2VxdW9pYS1wZ3Aub3Jn/HRQIjoH
	ar9G/FvWXBTpnSZxLUkEluT52m3ZdAbY+BUDFQoIApkBApsBAh4BFiEENswGWHsp
	llGmOvL/yX0ZjrPCvA4AAGwUAQCAHhDL7+lOa9VpIDwAvdSInODlnlLBAb3VY2DL
	joxt/gEA7a6+0EGPRHHwUmiGM+vjeZA47lds/H7yrnCYVw1uyASIdQQQFggAHRYh
	BE0UBQZTpALXNocEnSQEyVRuFFNgBQJmKoXUAAoJECQEyVRuFFNgqmIBAONAY+xG
	4tf8rmkZj9iZntIscYHDBNGRGstUO2ghr7JMAP9ynmtbo+mpx7/eQDeRh1yeO8HD
	rRWrWxdm8Y0yxDIKALgzBGYqgUwWCSsGAQQB2kcPAQEHQNMntfVfDFcH+9p0rEiP
	+kxGwU72wFWciI5oGA+zl07XiQGFBBgWCgE3BYJmKoFMBYkBSLK0CRDJfRmOs8K8
	DkcUAAAAAAAeACBzYWx0QG5vdGF0aW9ucy5zZXF1b2lhLXBncC5vcmeCIVFAF73S
	mgOgxoZkdDgvtx7H64iUXJdrYfh7N/u5RwKbAr6gBBkWCgBvBYJmKoFMCRBVqvGS
	JsT20kcUAAAAAAAeACBzYWx0QG5vdGF0aW9ucy5zZXF1b2lhLXBncC5vcmfUSc07
	/MD75BLcKefEN4ibIfta0IUgPM7Z2ixxo3YWohYhBKmCv3XLIAbBznn+r1Wq8ZIm
	xPbSAABs7gEA8/iYR0TnccHhNmmIqhj5uHPS528drto/WBJldpOubCgBAOEl7gEY
	QecDRe1PAE40LYwrFDREXDDe0a66lwUHR1AMFiEENswGWHspllGmOvL/yX0ZjrPC
	vA4AAAGCAP9F/8NIiMiLhebf7PpSEuTec8DRtKea+Nb7oyrn7+Rh6wD+OPxTkoqa
	HetYg4J8l7Z+jOM9/LNfPCZAvnyXGh/5BgW4OARmKoFMEgorBgEEAZdVAQUBAQdA
	YTjFZKL8lSJLbIWafbLclKFxt0FA6TIk832VsO0HjBIDAQgHiMYEGBYKAHgFgmYq
	gUwFiQFIsrQJEMl9GY6zwrwORxQAAAAAAB4AIHNhbHRAbm90YXRpb25zLnNlcXVv
	aWEtcGdwLm9yZ0OcXDVEWCNgjtcHOpnr/9j0M4iHQ3bcf8rpODTgxOHcApsMFiEE
	NswGWHspllGmOvL/yX0ZjrPCvA4AAPtsAQDjncr7Pl/9PaDPC6C0bp2VX/toVlpR
	X8BDNyb3ctRVGwEAtDSVcXKdpKqUcyoC4pcuv7pK9i4k6cjUAsdRFK814wE=
	=rTpE
	-----END PGP PUBLIC KEY BLOCK-----

### Applying for Visa at South Korea Diplomatic Missions nearby

#### Find South Korean Diplomatic mission nearby and check required documents
The first thing to do is to find which South Korean diplomatic mission you will need to visit to apply for the visa. [This can be done on this webpage.](https://www.visa.go.kr/openPage.do?MENU_ID=10106) Since required documents vary by diplomatic mission, it's important to know where you should visit based on your residency and place of issue on your passport. In case there are no South Korean diplomatic missions in the country you live in, You will need to check other nearby countries instead.

Once you find it, Visit the website of the corresponding South Korean diplomatic mission then find Visa information. Information for Visa application can be usually found under the `Consular / Visa` section (or a section with a similar name). In some regions, the Visa issuance service is delegated to the Visa application center. In that case, You'll find information that guides you to visit the application center website.

After you get to the page that gives you detailed information on what documents are required to apply for a C-3-1 visa (or Short-team general visa). Please check if you can prepare all the required documents. Some diplomatic missions require notarized documents or your flight and accommodation booking confirmation in advance.

#### Preparing your Visa application form

While you will be fill out other section of Visa application form by yourself based on information about yourself, Please follow the guidance below for following sections.
- 2. 신청 사증 정보/ DETAILS OF VISA APPLICATION
- 8. 방문정보 / DETAILS OF VISIT: From 8.1 to 8.5
- 9. 초청 정보 / DETAILS OF INVITATION
- 10. 방문경비 / FUNDING DETAILS

##### 2. 신청 사증 정보/ DETAILS OF VISA APPLICATION

<img class="img img-fluid" src="{% static "img/visa-form-section-2.png" %}">

- 2.1 체류기간(장ㆍ단기) Period of Stay(Long/Short-term): Please put check mark on `90일 이하 단기체류 Short-term Stay less than 90 days`
- 2.2 체류자격 Status of Stay: Enter `C-3-1`

##### 8. 방문정보 / DETAILS OF VISIT

<img class="img img-fluid" src="{% static "img/visa-form-section-8.png" %}">

- 8.1 입국목적 Purpose of Visit to Korea: Please put check mark on `행사참석/Meeting, Conference`
- 8.2 체류예정기간 Intended Period of Stay: Enter how many days you will stay in Korea. e.g. `16 DAYS`
- 8.3 체류예정기간 Intended Period of Stay: Enter when do you plan to arrive in Korea. e.g. `2024/07/20`
- 8.4 체류예정지(호텔 포함) Address in Korea (including hotels)
    - If you will be staying at Conference-provided accommodation, enter this address: `Sejong-1 gwan, Student's Dormitory, 45, Yongso-ro, Nam-gu, Busan, Republic of Korea`
    - If you will be staying accomodation arranged by yourself, enter address of that accomodation.
- 8.5 한국 내 연락처 Contact No. in Korea
    - If you have Korean SIM Card that can receive phone call, You may enter that phone number.
    - If you don't have Korean phone number and will be staying at Hotel arranged by yourself, You may enter phone number of that Hotel.
    - If you don't have Korean phone number and will be staying at Conference-provided accommodation, You may enter `+82-10-9978-7433` (Phone number of the local team chief)

##### 9. 초청 정보 / DETAILS OF INVITATION

<img class="img img-fluid" src="{% static "img/visa-form-section-9.png" %}">

- 9.1 초청인/초청회사 Is there anyone inviting the applicant for the visa?: Please put check mark on `예 Yes`
    - a) 초청인/초청회사명 Name of inviting person/organization (Korean, foreign resident in Korea, company, or institute): Enter `doubleO Co., Ltd.`
    - b) 생년월일/사업자등록번호 Date of Birth/Business Registration No.: Enter the business registration number of `doubleO Co., Ltd.` - Please refer to the business registration that we'll provide to you. (Note: This is in `XXX-XX-XXXXX` number format.)
    - c) 관계 Relationship to the applicant: Enter `EVENT HOST`
    - d) 주소 Address: Enter address of `doubleO Co., Ltd.` - Please refer to the business registration that we'll provide to you.
    - e) 전화번호 Phone No.: Enter Phone number of `doubleO Co., Ltd` which is `+82-51-623-2022`

##### 10. 방문경비 / FUNDING DETAILS

<img class="img img-fluid" src="{% static "img/visa-form-section-10.png" %}">

- 10.1 방문경비(미국 달러 기준) Estimated travel costs(in US dollars)
    - Enter total estimated travel costs in USD regardless funded by yourself or with help from others (such as bursaries, employer, family member) which would be including long distance transportation fee(such as flight, train, etc.), accommodation, food and other.
    - For example, Let's say you will book round trip flight that costs 800 USD, and will stay in Conference-provided accomodation for 7 nights while staying in Korea. That would be `800 USD (Flight)` + `21 USD * 7 (Accomodation)` + = `Total 1147 USD`. So in this case, you may enter `1147 USD` for this field. 
- 10.2 경비지급자 Who will pay for the applicant’s travel-related expenses? (Any relevant person including the applicant and/or organization)
    - If your travel is funded by yourself, You can skip this form.
    - If your travel is funded with DebConf Bursary, fill in the form as followings.
        - a) 성명/회사(단체)명 Name of Person/Organization (Company): Enter `Software in the Public Interest, Inc.` (The actual legal entity that will provide travel fund)
        - b) 관계 Relationship to the applicant: Enter `EVENT HOST`
        - c) 지원내용 Type of Support: Enter items you'll be funded with bursary. e.g. Flight ticket, Accommodation, Food
        - d) 연락처 Contact No.: Enter `+1-929-341-0248` (Contact number of Software in the Public Interest, Inc.)
    - If your travel is funded by other organization or person (such as Your employer, Your school(or university), Scholarship institution, Your friend or family), please fill in based on your own information. 

#### Submitting your Visa application

Depending on your situation, You'll be either sending your visa application and supporting documents through a courier service such as EMS, DHL or FedEx. or submitting the visa application on-site at either one of the nearby Korean missions, Visa application center or Travel agencies designated by Korean missions. There could be also cases that you're required to book an appointment for applying for visa. In that case, make sure to book an appointment earlier to apply and get your visa issued on time.

If there is no Korean mission in your region or country, you'll be applying to Korean mission in a nearby country that has the jurisdiction. In this case, there are special procedures such as paying the visa application fee with wire transfer, and sending the documents via DHL or FedEx couriers. 

After submitting your visa application, You can check application review status at the Visa Portal website. [Click here to visit the page for searing your application status.](https://www.visa.go.kr/openPage.do?MENU_ID=10301) Then, If you enter your information and click "Search" button, that would display current status of your application.

<img class="img img-fluid" src="{% static "img/visa-check.png" %}">

### Get your visa grant notice

Once your visa application is approved, you can download your visa online the same way you used to check your visa application status. After searching application status and you see visa approval status, Click "Certificate" button to download "Visa Grant Notice". Korean visas are not stickers placed in your passport. "Visa Grant Notice" you downloaded from the website is the visa itself. Once you have downloaded it, print and carry it with your passport during your trip to South Korea.

<img class="img img-fluid" src="{% static "img/visa-result.png" %}">

For reference, This is how Visa Grant Notice looks like.

<img class="img img-fluid" src="{% static "img/visa-sample.png" %}">