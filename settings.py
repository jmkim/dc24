# -*- encoding: utf-8 -*-
from pathlib import Path
from datetime import date
from decimal import Decimal

from django.urls import reverse_lazy

from debconf.settings import *

TIME_ZONE = 'Asia/Seoul'
USE_L10N = False
TIME_FORMAT = 'G:i'
SHORT_DATE_FORMAT = 'Y-m-d'
DATETIME_FORMAT = 'Y-m-d H:i:s'
SHORT_DATETIME_FORMAT = 'Y-m-d H:i'

try:
    from localsettings import *
except ImportError:
    pass

root = Path(__file__).parent

INSTALLED_APPS = (
    'dc24',
    'debconf.themes.cerulean',
    'news',
    'volunteers',
) + INSTALLED_APPS

STATIC_ROOT = str(root / 'localstatic/')

STATICFILES_DIRS = (
    str(root / 'static'),
)

STATICFILES_STORAGE = (
    'django.contrib.staticfiles.storage.ManifestStaticFilesStorage')

TEMPLATES[0]['DIRS'] = TEMPLATES[0]['DIRS'] + (
    str(root / 'templates'),
)

TEMPLATES[0]['OPTIONS']['context_processors'] += (
    'dc24.context_processors.expose_wafer_talks',
    'dc24.context_processors.is_it_debconf'
)

WAFER_MENUS += (
    {
        'menu': 'about',
        'label': 'About',
        'items': [
            {
                'menu': 'coc',
                'label': 'Code of Conduct',
                'url': reverse_lazy('wafer_page', args=('about/coc',)),
            },
            {
                'menu': 'debconf',
                'label': 'DebConf',
                'url': reverse_lazy('wafer_page', args=('about/debconf',)),
            },
            {
                'menu': 'debcamp',
                'label': 'DebCamp',
                'url': reverse_lazy('wafer_page', args=('about/debcamp',)),
            },
            {
                'menu': 'debian',
                'label': 'Debian',
                'url': reverse_lazy('wafer_page', args=('about/debian',)),
            },
            {
                'menu': 'accommodation',
                'label': 'Accommodation',
                'url': reverse_lazy('wafer_page', args=('about/accommodation',)),
            },
            {
                'menu': 'bursaries',
                'label': 'Bursaries',
                'url': reverse_lazy('wafer_page', args=('about/bursaries',)),
            },
            {
                'menu': 'child-care',
                'label': 'Child care',
                'url': reverse_lazy('wafer_page', args=('about/childcare',)),
            },
            {
                'menu': 'COVID-19',
                'label': 'COVID-19',
                'url': reverse_lazy('wafer_page', args=('about/covid19',)),
            },
            {
                'menu': 'faq',
                'label': 'FAQ',
                'url': 'https://wiki.debian.org/DebConf/24/Faq',
            },
            {
                'menu': 'Busan',
                'label': 'Busan',
                'url': reverse_lazy('wafer_page', args=('about/busan',)),
            },
            {
                'menu': 'registration_information',
                'label': 'Registration Information',
                'url': reverse_lazy('wafer_page', args=('about/registration',)),
            },
            {
                'menu': 'ksp',
                'label': 'Key-signing party',
                'url': reverse_lazy('wafer_page', args=('about/ksp',)),
            },
            {
                'menu': 'touristguide',
                'label': 'Tourist Guide',
                'url': 'https://wiki.debian.org/DebConf/24/TouristGuide'
            },
            {
                'menu': 'venue',
                'label': 'Venue',
                'url': reverse_lazy('wafer_page', args=('about/venue',)),
            },
            {
                'menu': 'visas',
                'label': 'Visas',
                'url': reverse_lazy('wafer_page', args=('about/visas',)),
            },
            {
                'menu': 'contact_link',
                'label': 'Contact us',
                'url': reverse_lazy('wafer_page', args=('contact',)),
            },
        ],
    },
    {
        'menu': 'sponsors_index',
        'label': 'Sponsors',
        'items': [
            {
                'menu': 'become_sponsor',
                'label': 'Become a Sponsor',
                'url': reverse_lazy('wafer_page', args=('sponsors/become-a-sponsor',)),
            },
            {
                'menu': 'sponsors',
                'label': 'Our Sponsors',
                'url': reverse_lazy('wafer_sponsors'),
            },
        ],
    },
    {
        'menu': 'schedule_index',
        'label': 'Schedule',
        'items': [
            {
                'menu': 'cfp',
                'label': 'Call for Proposals',
                'url': reverse_lazy('wafer_page', args=('cfp',)),
            },
            {
                'menu': 'confirmed_talks',
                'label': 'Confirmed Talks',
                'url': reverse_lazy('wafer_users_talks'),
            },
            {
                'menu': 'important-dates',
                'label': 'Important Dates',
                'url': reverse_lazy('wafer_page', args=('schedule/important-dates',)),
            },
            {
                'menu': 'mobile_schedule',
                'label': 'Mobile-Friendly Schedule',
                'url': reverse_lazy('wafer_page', args=('schedule/mobile',)),
            },
            {
                'menu': 'schedule',
                'label': 'Schedule',
                'url': reverse_lazy('wafer_full_schedule'),
            },
        ],
    },
    {
        'menu': 'wiki_link',
        'label': 'Wiki',
        'url': 'https://wiki.debian.org/DebConf/24',
    },
    {
        'menu': 'register',
        'label': 'Register',
        'url': reverse_lazy('register'),
    },
    {
        'menu': 'volunteers',
        'label': 'Volunteer!',
        'url': reverse_lazy('wafer_tasks'),
    },
    {
        'menu': 'korean_index',
        'label': 'Korean(한국어)',
        'items': [
            {
                'menu': 'ko_overview',
                'label': '개요',
                'url': reverse_lazy('wafer_page', args=('ko/overview',)),
            },
            {
                'menu': 'ko_program',
                'label': '프로그램',
                'url': reverse_lazy('wafer_page', args=('ko/debconf',)),
            },
            {
                'menu': 'ko_social',
                'label': '소셜 행사',
                'url': reverse_lazy('wafer_page', args=('ko/social',)),
            },
            {
                'menu': 'ko_register',
                'label': '등록 안내',
                'url': reverse_lazy('wafer_page', args=('ko/register',)),
            },
            {
                'menu': 'ko_local_team',
                'label': '로컬 팀',
                'url': reverse_lazy('wafer_page', args=('ko/local-team',)),
            },
            {
                'menu': 'ko_volunteer',
                'label': '자원 봉사',
                'url': reverse_lazy('wafer_page', args=('ko/volunteer',)),
            },
            {
                'menu': 'ko_network',
                'label': '네트워크',
                'url': reverse_lazy('wafer_page', args=('ko/network',)),
            },
            {
                'menu': 'press_release',
                'label': '언론보도',
                'url': 'https://docs.google.com/spreadsheets/d/1G96ojzOu6HnzIGodYKlcchZav5YNKAI8WQArbyxsLcU',
            },
        ],
    },
)

WAFER_DYNAMIC_MENUS = ()

ROOT_URLCONF = 'urls'

CRISPY_TEMPLATE_PACK = 'bootstrap5'
CRISPY_FAIL_SILENTLY = not DEBUG

DEFAULT_FROM_EMAIL = 'registration@debconf.org'
REGISTRATION_DEFAULT_FROM_EMAIL = 'noreply@debconf.org'

WAFER_REGISTRATION_MODE = 'custom'
WAFER_USER_IS_REGISTERED = 'register.models.user_is_registered'

WAFER_VIDEO_REVIEWER = False
WAFER_VIDEO_LICENSE = 'DebConf Video License (MIT-like)'
WAFER_VIDEO_LICENSE_URL = 'https://video.debian.net/LICENSE'

WAFER_PUBLIC_ATTENDEE_LIST = False

WAFER_TALK_LANGUAGES = (
# If we support multiple talk languages, list them here:
#    ("en", "English"),
)

WAFER_CONFERENCE_ACRONYM = 'debconf24'
DEBCONF_ONLINE = False
DEBCONF_CITY = 'Busan'
DEBCONF_NAME = 'DebConf 24'
DEBCONF_CONFIRMATION_DEADLINE = date(2024, 6, 19)
DEBCONF_BURSARY_DEADLINE = date(2024, 5, 8)
DEBCONF_BURSARY_ACCEPTANCE_DEADLINE = date(2024, 6, 19)
DEBCONF_BILLING_CURRENCY = 'EUR'
DEBCONF_BILLING_CURRENCY_SYMBOL = '€'
DEBCONF_BURSARY_CURRENCY = 'USD'
DEBCONF_BURSARY_CURRENCY_SYMBOL = '$'
DEBCONF_COLLECT_AFFILIATION = True
DEBCONF_LOCAL_CURRENCY = 'KRW'
DEBCONF_LOCAL_CURRENCY_SYMBOL = '₩'
DEBCONF_LOCAL_CURRENCY_RATE = Decimal('1450.00')
DEBCONF_REVIEW_FREE_ATTENDEES = False
DEBCONF_BREAKFAST = True
DEBCONF_INVOICE_ADDRESS = """\
c/o Association Debian France
1 passage de Bretagne
Boîte 084
93110
Rosny-Sous-Bois
France"""

DEBCONF_DATES = (
    # Conference part, start date, end date
    ('DebCamp', date(2024, 7, 20), date(2024, 7, 27)),
    ('DebConf', date(2024, 7, 28), date(2024, 8, 4)),
)
DEBCONF_CONFERENCE_DINNER_DAY = date(2024, 8, 1)
DEBCONF_SKIPPED_MEALS = (
    # DebCamp arrival day has no catered food
    ('breakfast', date(2024, 7, 20)),
    ('lunch', date(2024, 7, 20)),
    ('dinner', date(2024, 7, 20)),
    # Day trip day, lunch will be not served at the venue
    ('lunch', date(2024, 7, 31)),
)

VOLUNTEERS_FIRST_DAY = date(2024, 7, 20)
VOLUNTEERS_LAST_DAY = date(2024, 8, 4)

DEBCONF_T_SHIRT_SIZES = (
    ('', 'No T-shirt'),
    ('', '—'),
    ('s', 'S - Small'),
    ('m', 'M - Medium'),
    ('l', 'L - Large'),
    ('xl', 'XL - Extra Large'),
    ('2xl', '2XL - Extra Large 2'),
    ('3xl', '3XL - Extra Large 3'),
    ('4xl', '4XL - Extra Large 4'),
    ('5xl', '5XL - Extra Large 5'),
    ('s_f', 'S - Small, Fitted cut'),
    ('m_f', 'M - Medium, Fitted cut'),
    ('l_f', 'L - Large, Fitted cut'),
    ('xl_f', 'XL - Extra Large, Fitted cut'),
    ('2xl_f', '2XL - Extra Large 2, Fitted cut'),
)
DEBCONF_SHOE_SIZES = ()

INVOICE_PREFIX = 'DC24-'

PRICES = {
    'fee': {
        '': {
            'name': 'Regular',
            'price': 0,
        },
        'pro': {
            'name': 'Professional',
            'price': 200,
        },
        'corp': {
            'name': 'Corporate',
            'price': 500,
        },
    },
    'meal': {
        'breakfast': {
            'price': 6,
        },
        'lunch': {
            'price': 6,
        },
        'dinner': {
            'price': 6,
        },
        'conference_dinner': {
            'price': 40,
        },
    },
    'accomm': {
        'shared': {
            'description': 'On-site shared room (Student dormitory)',
            'price': 21,
            'bursary': True,
        },
        'single': {
            'description': 'On-site private room (Student dormitory)',
            'price': 42,
        },
    },
    'daytrip': {
        'a': {
            'description': 'Traditional experiences in Busan',
            'long_description':
                'A morning visit to the Busan Traditional Culture Experience '
                'Center for traditional games, and rice cake making. '
                'After lunch, enjoy a scenic ride on the Blue Line '
                'Park beach train (Cheongsapo-Songjeong) and visit the Haedong '
                'Yonggungsa Temple.',
            'price': 21,
            'insurance_price': 4,
            'capacity': 60,
        },
        'b': {
            'description': 'Gyeongju tour',
            'long_description':
                "Exploring the historic Seokguram Grotto and Bulguksa Temple "
                "in the morning. After lunch, you'll enjoy a vibrant Hanbok "
                "experience at Hwangnidan-gil and visit the fascinating "
                "Daereungwon Tomb Complex. "
                "Your name, gender, and T-shirt size will be shared with the "
                "tour provider so they can provide the clothing for the Hanbok "
                "experience.",
            'price': 21,
            'insurance_price': 4,
            'capacity': 70,
        },
        'c': {
            'description': 'Ulsan tour',
            'long_description':
                "Exploring the serene Taehwagang Bamboo Forest and Daewangam "
                "Park in the morning, followed by a visit to the Ulsan Bridge "
                "Observatory and Jangsaengpo Whale Cultural Village. After "
                "lunch, you'll discover the traditional charm of Oegosan Onggi "
                "Village and end the day with a picturesque visit to "
                "Ganjeolgot.",
            'price': 14,
            'insurance_price': 4,
            'capacity': 70,
        },
    },
}

DEBCONF_TALK_PROVISION_URLS = {
    'etherpad': {
        'pattern': 'https://pad.dc24.debconf.org/p/{id}-{slug:.45}',
        'public': True,
    },
}

DEBCONF_VENUE_IRC_CHANNELS = ["#debconf24-{name}", "#debconf"]
DEBCONF_VENUE_STREAM_HLS_URL = "https://onsite.live.debconf.org/live/{name}.m3u8"
DEBCONF_VENUE_STREAM_RTMP_URL = "rtmp://onsite.live.debconf.org/front/{name}_{quality}"

PAGE_DIR = '%s/' % (root / 'pages')
NEWS_DIR = '%s/' % (root / 'news' / 'stories')
SPONSORS_DIR = '%s/' % (root / 'sponsors')

TRACKS_FILE = str(root / 'tracks.yml')
TALK_TYPES_FILE = str(root / 'talk_types.yml')

BAKERY_VIEWS += (
    'news.views.NewsItemView',
    'news.views.NewsFeedView',
    'news.views.NewsRSSView',
    'news.views.NewsAtomView',
    'volunteers.views.VolunteerStatisticsView',
)
BUILD_DIR = '%s/' % (root / 'static_mirror')
